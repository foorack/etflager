import 'package:etflager_api/src/config.dart';
import 'package:etflager_api/src/storage/storage.dart';
import 'package:test/test.dart';
// import 'package:etflager_common/src/api/model/product.dart';

void main() {
  Storage storage;

  setUp(() async {
    storage = Storage(StorageTestConfig());
    await storage.open();
  });

  tearDown(() async {
    await storage.close();
  });

  // test('Reset database', () async {
  //   await storage.addProduct('', false, false);
  //   expect(await storage.resetDatabase(), true);
  //   expect(await storage.getProducts(), []);
  // });

  // test('Product adding', () async {
  //   const testTitle = 'TEST # ABC123';
  //   int size = (await storage.getProducts()).length;
  //   Product product = await storage.addProduct(testTitle, false, false);
  //   expect((await storage.getProducts()).length, size + 1);
  //   expect((await storage.getProduct(product.artnr)).title, testTitle);
  // });
}

class StorageTestConfig extends Config {
  @override
  String mongodb_uri = 'mongodb://localhost:27017/etflagerTest';
}
