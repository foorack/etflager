import 'package:etflager_api/src/config.dart';
import 'package:etflager_api/src/storage/storage.dart';
import 'package:etflager_api/src/web/web.dart';

void main() async {
  print('Loading configuration...');
  Config config = Config();
  print('Configuration loaded.');

  print('Connecting to storage...');
  Storage storage = Storage(config);
  await storage.open();
  print('Storage connected.');

  print('DEBUG: Resetting database...');
  await storage.debugResetAndPopulate(100);
  print('DEBUG: Database reset.');

  print('Starting web server...');
  WebServer web = WebServer(config, storage);
  await web.start();
  print('Web server now running on port ${config.web_port}.');
}
