import 'dart:convert';
import 'dart:math' as math;

import 'package:angel_cors/angel_cors.dart';
import 'package:angel_framework/angel_framework.dart';
import 'package:angel_framework/http.dart';
import 'package:etflager_api/src/config.dart';
import 'package:etflager_api/src/storage/storage.dart';
import 'package:etflager_common/src/api/model/product.dart';
import 'package:etflager_common/src/api/model/product_image.dart';
import 'package:image/image.dart';
import 'package:sanitize_html/sanitize_html.dart' show sanitizeHtml;

class WebServer {
  Config config;
  Storage storage;

  WebServer(this.config, this.storage);

  Future<void> start() async {
    var app = Angel();
    var http = AngelHttp(app);

    app.fallback(cors());

    app.group('/api/v1/', (router) {
      router
        ..get('/products', (req, res) async {
          res.writeln(jsonEncode((await storage.getProducts(
                  search: req.queryParameters['search'],
                  page: req.queryParameters['page'],
                  per_page: req.queryParameters['per_page']))
              .map(Product.encode)
              .toList()));
        })
        ..post('/products', (req, res) async {
          await req.parseBody();

          if (!requireFields(req, res, Product.encode(Product.empty()))) {
            return;
          }

          Product product = Product.decode(req.bodyAsMap);

          // Prevent XSS
          product.title = sanitizeHtml(product.title);
          product.shortDescription = sanitizeHtml(product.shortDescription);
          product.longDescription = sanitizeHtml(product.longDescription);

          product = await storage.addProduct(product);

          res.writeln(jsonEncode(Product.encode(product)));
        })
        ..put('/products', (req, res) async {
          await req.parseBody();

          if (!requireFields(req, res, Product.encode(Product.empty()))) {
            return;
          }

          Product product = Product.decode(req.bodyAsMap);

          if (await storage.getProduct(product.artnr) != null) {
            // Prevent XSS
            product.title = sanitizeHtml(product.title);
            product.shortDescription = sanitizeHtml(product.shortDescription);
            product.longDescription = sanitizeHtml(product.longDescription);

            await storage.updateProduct(product);
            res.writeln(jsonEncode(Product.encode(product)));
          } else {
            res.writeln(
                jsonEncode({'code': 404, 'message': 'Product not found'}));
          }
        })
        ..get('/products/:artnr', (req, res) async {
          int artnr = int.tryParse(req.params['artnr']);
          var product = await storage.getProduct(artnr);
          if (product != null) {
            res.writeln(jsonEncode(Product.encode(product)));
          } else {
            res.writeln(
                jsonEncode({'code': 404, 'message': 'Product not found'}));
          }
        })
        ..delete('/products/:artnr', (req, res) async {
          int artnr = int.tryParse(req.params['artnr']);
          var product = await storage.getProduct(artnr);
          if (product != null) {
            await storage.deleteProduct(artnr);
            res.writeln(jsonEncode(Product.encode(product)));
          } else {
            res.writeln(
                jsonEncode({'code': 404, 'message': 'Product not found'}));
          }
        })
        ..get('/images', (req, res) async {
          res.writeln(jsonEncode((await storage.getImages(
                  search: req.queryParameters['search'],
                  page: req.queryParameters['page'],
                  per_page: req.queryParameters['per_page']))
              .map(ProductImage.encode)
              .toList()));
        })
        ..post('/images', (req, res) async {
          await req.parseBody();

          if (!requireFields(
              req, res, ProductImage.encode(ProductImage.empty()))) {
            return;
          }

          ProductImage image = ProductImage.decode(req.bodyAsMap);

          // Prevent XSS
          image.name = sanitizeHtml(image.name);

          // Resize to 1024x1024 PNG
          Image img = decodeImage(base64.decode(image.imageData));
          img = copyResize(img, width: 1024, height: 1024);
          image.imageData = base64.encode(encodePng(img));

          image = await storage.addImage(image);

          res.writeln(jsonEncode(ProductImage.encode(image)));
        })
        ..put('/images', (req, res) async {
          await req.parseBody();

          if (!requireFields(
              req, res, ProductImage.encode(ProductImage.empty()))) {
            return;
          }

          ProductImage image = ProductImage.decode(req.bodyAsMap);

          if (await storage.getImage(image.imgnr) != null) {
            // Prevent XSS
            image.name = sanitizeHtml(image.name);

            // Resize to 1024x1024 PNG
            Image img = decodeImage(base64.decode(image.imageData));
            img = copyResize(img, width: 1024, height: 1024);
            image.imageData = base64.encode(encodePng(img));

            await storage.updateImage(image);
            res.writeln(jsonEncode(ProductImage.encode(image)));
          } else {
            res.writeln(
                jsonEncode({'code': 404, 'message': 'Image not found'}));
          }
        })
        ..delete('/images/:imgnr', (req, res) async {
          int imgnr = int.tryParse(req.params['imgnr']);
          var image = await storage.getImage(imgnr);
          if (image != null) {
            await storage.deleteImage(imgnr);
            res.writeln(jsonEncode(ProductImage.encode(image)));
          } else {
            res.writeln(
                jsonEncode({'code': 404, 'message': 'Image not found'}));
          }
        });
    });
    await http.startServer(config.web_host, config.web_port);
  }

  bool requireFields(
      RequestContext req, ResponseContext res, Map<String, dynamic> fields,
      [String namespace = ""]) {
    for (String fullField in fields.keys) {
      String field = fullField.split('.').last;
      String fieldspace = fullField.substring(
          0, math.max(fullField.length - field.length - 1, 0));
      if (fieldspace != namespace) {
        continue;
      }
      if (!req.bodyAsMap.containsKey(field)) {
        res.writeln(jsonEncode({
          'code': 400,
          'message': 'Missing field $namespace$field in body'
        }));
        return false;
      }
      if (req.bodyAsMap[field] == null) {
        res.writeln(jsonEncode({
          'code': 400,
          'message': 'Field $namespace$field must not be null'
        }));
        return false;
      }
      if (req.bodyAsMap[field].runtimeType != fields[field].runtimeType &&
          (fields[field].runtimeType == 0.0.runtimeType &&
              req.bodyAsMap[field].runtimeType != 0.runtimeType)) {
        res.writeln(jsonEncode({
          'code': 400,
          'message':
              'Field $namespace$field is not a ${fields[field].runtimeType.toString()}'
        }));
        return false;
      }
      // Check sub-maps with dot notation
      if (req.bodyAsMap[field].runtimeType == {}.runtimeType) {
        if (!requireFields(req, res, fields, namespace + field + ".")) {
          return false;
        }
      }
    }
    return true;
  }
}
