class Config {

    // Web
    String web_host = 'localhost';
    int web_port = 3000;

    // MongoDB
    String mongodb_uri = 'mongodb://localhost:27017/etflager';

}