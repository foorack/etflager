import 'dart:math';

import 'package:english_words/english_words.dart';
import 'package:etflager_api/src/config.dart';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:etflager_common/src/api/model/product.dart';
import 'package:etflager_common/src/api/model/product_image.dart';
import 'package:etflager_common/src/api/model/product_stock_status.dart';

class Storage {
  Config config;
  Db db;

  Storage(this.config);

  Future open() {
    db = new Db(config.mongodb_uri);
    return db.open();
  }

  Future close() {
    assert(db != null);
    return db.close();
  }

  Future debugResetAndPopulate(int n) async {
    await resetDatabase();
    Random r = Random();
    List<Future> futures = List();
    for (int i = 0; i != n; i++) {
      bool stock = r.nextInt(3) != 0;
      futures.add(addProduct(Product(
          null,
          generateWordPairs().take(r.nextInt(5) + 1).join(" "),
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius dictum nisl.',
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius dictum nisl. Mauris bibendum venenatis maximus. Pellentesque faucibus elit tristique orci lobortis facilisis. Sed sit amet nulla in arcu consequat vehicula eget non turpis. Nullam vel auctor purus, eu hendrerit diam. Mauris massa arcu, efficitur sit amet condimentum sit amet, euismod eget nisi. Nulla quis sagittis metus. Ut quis justo in dolor bibendum fringilla a a nunc. Nullam at risus diam. Sed aliquet, risus ac sodales finibus, ipsum orci facilisis turpis, quis varius libero nibh at dolor. Nunc mollis arcu sit amet mauris congue fringilla. Etiam pellentesque ut ligula eget egestas. Sed dictum aliquet magna, vel lacinia metus. Vivamus luctus ac orci vitae efficitur. Integer nisi enim, euismod at vulputate eget, porttitor sit amet nibh.',
          ProductStockStatus(stock, stock ? false : r.nextBool()),
          r.nextInt(150) + 20.0,
          r.nextBool(),
          ProductImage.empty())));
    }
    await Future.wait(futures);
  }

  Future resetDatabase() {
    return db.drop();
  }

  Future<List<Product>> getProducts(
      {String search, int page, int per_page}) async {
    if (page == null || page < 1) {
      page = 1;
    }
    if (per_page == null || per_page < 1) {
      per_page = 1;
    }
    if (per_page > 50) {
      per_page = 50;
    }
    var col_products = db.collection('products');
    var query = where;
    if (search != null) {
      query =
          query.match('title', RegExp.escape(search), caseInsensitive: true);
    }
    query = query.skip((page - 1) * per_page).limit(per_page);
    return await col_products
        .find(query)
        .asyncMap((obj) async {
          obj['image'] =
              ProductImage.encode(await getImage(obj['imgnr'], empty: true));
          return obj;
        })
        .map((obj) {
          obj.remove('imgnr');
          return obj;
        })
        .map(Product.decode)
        .toList();
  }

  Future<Product> getProduct(int artnr) async {
    assert(artnr != null);
    var col_products = db.collection('products');
    var object = await col_products.findOne({'artnr': artnr});
    if (object == null) {
      return null;
    }
    // Product does not keep image directly, it is "joined" from image collection
    object['image'] =
        ProductImage.encode(await getImage(object['imgnr'], empty: true));
    return Product.decode(object);
  }

  Future<Product> addProduct(Product product) async {
    assert(product.title != null);
    assert(product.shortDescription != null);
    assert(product.longDescription != null);
    assert(product.stockStatus.available != null);
    assert(product.stockStatus.unknown != null);
    assert(product.stockStatus.unknown ? !product.stockStatus.available : true);
    assert(product.price != null);
    assert(product.priceUnknown != null);
    assert(product.image != null);

    var col_products = db.collection('products');
    product.artnr = await findAvailableArtnr();
    var object = Product.encode(product);

    // Product does not keep image directly, it is "joined" from image collection
    object.remove('image');
    object['imgnr'] = product.image.imgnr;
    product.image = await getImage(product.image.imgnr, empty: true);

    await col_products.insert(object);

    return product;
  }

  Future<bool> updateProduct(Product product) async {
    assert(product.title != null);
    assert(product.shortDescription != null);
    assert(product.longDescription != null);
    assert(product.stockStatus.available != null);
    assert(product.stockStatus.unknown != null);
    assert(product.stockStatus.unknown ? !product.stockStatus.available : true);
    assert(product.price != null);
    assert(product.priceUnknown != null);

    var col_products = db.collection('products');
    var object = Product.encode(product);

    // Product does not keep image directly, it is "joined" from image collection
    object.remove('image');
    object['imgnr'] = product.image.imgnr;
    product.image = await getImage(product.image.imgnr, empty: true);

    var res =
        await col_products.update(where.eq('artnr', product.artnr), object);
    return res['err'] != null && res['n'] > 0;
  }

  Future<bool> deleteProduct(int artnr) async {
    assert(artnr != null);
    var col_products = db.collection('products');
    var res = await col_products.remove({'artnr': artnr});
    return res['err'] != null && res['n'] > 0;
  }

  Future<ProductImage> addImage(ProductImage image) async {
    assert(image.name != null);
    assert(image.imageData != null);

    var col_images = db.collection('images');
    image.imgnr = await findAvailableImgnr();
    await col_images.insert(ProductImage.encode(image));

    return image;
  }

  Future<List<ProductImage>> getImages(
      {String search, int page, int per_page}) async {
    if (page == null || page < 1) {
      page = 1;
    }
    if (per_page == null || per_page < 1) {
      per_page = 1;
    }
    if (per_page > 50) {
      per_page = 50;
    }
    var col_images = db.collection('images');
    return await col_images
        .find(where.skip((page - 1) * per_page).limit(per_page))
        .map(ProductImage.decode)
        .toList();
  }

  Future<ProductImage> getImage(int imgnr, {empty = false}) async {
    assert(imgnr != null);
    var col_images = db.collection('images');
    var object = await col_images.findOne({'imgnr': imgnr});
    return object == null
        ? (empty ? ProductImage.empty() : null)
        : ProductImage.decode(object);
  }

  Future<bool> updateImage(ProductImage image) async {
    assert(image.imgnr != null);
    assert(image.name != null);
    assert(image.imageData != null);

    var col_images = db.collection('images');
    var res = await col_images.update(
        where.eq('imgnr', image.imgnr), ProductImage.encode(image));
    return res['err'] != null && res['n'] > 0;
  }

  Future<bool> deleteImage(int images) async {
    assert(images != null);
    var col_images = db.collection('images');
    var res = await col_images.remove({'imgnr': images});
    return res['err'] != null && res['n'] > 0;
  }

  Future<int> findAvailableArtnr() async {
    Product product;
    int artnr;
    do {
      artnr = _generateId();
      product = await getProduct(artnr);
    } while (product != null);
    return artnr;
  }

  Future<int> findAvailableImgnr() async {
    ProductImage image;
    int imgnr;
    do {
      imgnr = _generateId();
      image = await getImage(imgnr);
    } while (image != null);
    return imgnr;
  }

  int _generateId() => Random().nextInt(99999999 - 1 - 10000000) + 1 + 10000000;
}
