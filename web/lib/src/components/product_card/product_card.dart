import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:etflager_common/src/api/model/product.dart';
import 'package:etflager_web/src/components/product_card/product_card_price.dart';
import 'package:etflager_web/src/components/product_stock_status/product_stock_status.dart';

import 'package:etflager_web/src/route_paths.dart';

@Component(
  selector: 'product-card',
  styleUrls: ['product_card.css'],
  templateUrl: 'product_card.html',
  directives: [NgIf, routerDirectives, ProductStockStatusComponent, ProductCardPriceComponent],
  exports: [RoutePaths],
)
class ProductCardComponent {
  @Input()
  Product product;

  String getProductPath() {
    return RoutePaths.product.toUrl(parameters: {'artnr': product.artnr.toString()});
  }
}
