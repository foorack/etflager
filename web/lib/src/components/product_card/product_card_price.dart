import 'package:angular/angular.dart';
import 'package:etflager_common/src/api/model/product.dart';

@Component(
  selector: 'product-card-price',
  template: '{{product.priceUnknown ? "??? kr" : product.price.toString() + " kr"}}',
  directives: [],
)
class ProductCardPriceComponent {
  @Input()
  Product product;
}
