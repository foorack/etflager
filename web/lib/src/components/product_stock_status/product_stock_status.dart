import 'package:angular/angular.dart';
import 'package:etflager_common/src/api/model/product_stock_status.dart';

@Component(
  selector: 'product-stock-status',
  styleUrls: ['product_stock_status.css'],
  templateUrl: 'product_stock_status.html',
  directives: [NgIf],
)
class ProductStockStatusComponent {
  @Input()
  ProductStockStatus stockStatus;
}
