import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

import 'package:etflager_web/src/route_paths.dart';

@Component(
  selector: 'header',
  styleUrls: ['header.css'],
  templateUrl: 'header.html',
  directives: [routerDirectives],
  exports: [RoutePaths],
)
class HeaderComponent {
  final Router _router;
  HeaderComponent(this._router);

  void searchFor(String query) async {
    if (query.isNotEmpty) {
      await _router.navigate(RoutePaths.search.toUrl(),
          NavigationParams(queryParameters: {"q": query}));
    }
  }
}
