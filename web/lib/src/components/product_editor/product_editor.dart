import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:angular_router/angular_router.dart';
import 'package:etflager_common/src/api/model/product.dart';
import 'package:etflager_web/app.dart';
import 'package:etflager_web/src/route_paths.dart';

@Component(
  selector: 'product-editor',
  styleUrls: ['product_editor.css'],
  templateUrl: 'product_editor.html',
  directives: [routerDirectives, formDirectives],
  exports: [RoutePaths],
)
class ProductEditorComponent implements OnInit {
  final Location _location;
  ProductEditorComponent(this._location);

  @Input()
  Product product;

  @override
  void ngOnInit() {
    if (product == null) {
      product = Product.empty();
    }
  }

  void stockStatusChange(int id) {
    if (id == 1) {
      product.stockStatus.available = true;
      product.stockStatus.unknown = false;
    } else if (id == 2) {
      product.stockStatus.available = false;
      product.stockStatus.unknown = false;
    } else if (id == 3) {
      product.stockStatus.available = false;
      product.stockStatus.unknown = true;
    } else {
      throw ArgumentError.value(id);
    }
  }

  void formSubmit() async {
    if (product.title == null || product.title.isEmpty) {
      return;
    }
    if (product.shortDescription == null || product.shortDescription.isEmpty) {
      return;
    }
    if (product.longDescription == null || product.longDescription.isEmpty) {
      return;
    }
    if (product.price == null || product.price < 0 || product.price > 99999) {
      return;
    }
    if (product.image.imgnr == null || product.image.imgnr < 0 || product.image.imgnr > 99999999) {
      return;
    }
    if (product.artnr == null) {
      await ETFLagerApp.api.addProduct(product);
      product = Product.empty();
    } else {
      await ETFLagerApp.api.updateProduct(product);
      _location.back();
    }
  }
}
