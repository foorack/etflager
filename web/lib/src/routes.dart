import 'package:angular_router/angular_router.dart';

import 'package:etflager_web/src/pages/home_page/home_page.template.dart'
    as home_page_template;
import 'package:etflager_web/src/pages/search_page/search_page.template.dart'
    as search_page_template;
import 'package:etflager_web/src/pages/product_page/product_page.template.dart'
    as product_page_template;
import 'package:etflager_web/src/pages/admin_page/admin_page.template.dart'
    as admin_page_template;
import 'package:etflager_web/src/pages/admin_product_page/admin_product_page.template.dart'
    as admin_product_page_template;
import 'package:etflager_web/src/pages/admin_product_add_page/admin_product_add_page.template.dart'
    as admin_product_add_page_template;
import 'package:etflager_web/src/pages/admin_product_edit_page/admin_product_edit_page.template.dart'
    as admin_product_edit_page_template;
import 'package:etflager_web/src/pages/admin_image_page/admin_image_page.template.dart'
    as admin_image_page_template;
import 'package:etflager_web/src/pages/admin_image_upload_page/admin_image_upload_page.template.dart'
    as admin_image_upload_page_template;
import 'route_paths.dart';

export 'route_paths.dart';

class Routes {
  static final home = RouteDefinition(
    routePath: RoutePaths.home,
    component: home_page_template.HomePageComponentNgFactory,
  );
  static final search = RouteDefinition(
    routePath: RoutePaths.search,
    component: search_page_template.SearchPageComponentNgFactory,
  );
  static final product = RouteDefinition(
    routePath: RoutePaths.product,
    component: product_page_template.ProductPageComponentNgFactory,
  );

  static final admin_home = RouteDefinition(
    routePath: RoutePaths.admin_home,
    component: admin_page_template.AdminPageComponentNgFactory,
  );
  static final admin_products = RouteDefinition(
    routePath: RoutePaths.admin_products,
    component: admin_product_page_template.AdminProductPageComponentNgFactory,
  );
  static final admin_products_add = RouteDefinition(
    routePath: RoutePaths.admin_products_add,
    component:
        admin_product_add_page_template.AdminProductAddPageComponentNgFactory,
  );
  static final admin_products_edit = RouteDefinition(
    routePath: RoutePaths.admin_products_edit,
    component:
        admin_product_edit_page_template.AdminProductEditPageComponentNgFactory,
  );
  static final admin_images = RouteDefinition(
    routePath: RoutePaths.admin_images,
    component: admin_image_page_template.AdminImagePageComponentNgFactory,
  );
  static final admin_images_upload = RouteDefinition(
    routePath: RoutePaths.admin_images_upload,
    component:
        admin_image_upload_page_template.AdminImageUploadPageComponentNgFactory,
  );

  static final all = <RouteDefinition>[
    home,
    search,
    product,
    admin_home,
    admin_products,
    admin_products_add,
    admin_products_edit,
    admin_images,
    admin_images_upload,
  ];
}
