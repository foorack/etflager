import 'dart:async';
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:angular_router/angular_router.dart';
import 'package:etflager_web/app.dart';
import 'package:etflager_web/src/route_paths.dart';
import 'package:etflager_common/src/api/model/product_image.dart';

@Component(
  selector: 'admin-image-page',
  styleUrls: ['admin_image_page.css'],
  templateUrl: 'admin_image_page.html',
  directives: [NgIf, NgFor, routerDirectives, formDirectives],
  exports: [RoutePaths],
)
class AdminImagePageComponent implements OnActivate {
  List<ProductImage> images = [
    ProductImage.empty(),
    ProductImage.empty(),
    ProductImage.empty(),
    ProductImage.empty()
  ];
  bool loading = true;
  int editing;
  String imageName;

  @override
  Future<void> onActivate(RouterState previous, RouterState current) async {
    images = await ETFLagerApp.api.getImages();
    loading = false;

    _updateScrollInfo([_]) {
      if (window.innerHeight + window.scrollY >= document.body.scrollHeight - 150) {
        print("at bottom");
      }
    }

    window.onScroll.listen(_updateScrollInfo);
    window.onResize.listen(_updateScrollInfo);
    _updateScrollInfo();
  }

  void saveImage(ProductImage image) async {
    image.name = imageName;
    await ETFLagerApp.api.updateImage(image);
    cancelEdit();
  }

  void editImage(int imgnr, String name) {
    editing = imgnr;
    imageName = name;
  }

  void cancelEdit() {
    editing = null;
    imageName = null;
  }

  void deleteImage(int imgnr) async {
    await ETFLagerApp.api.deleteImage(imgnr);
    images = await ETFLagerApp.api.getImages();
  }
}
