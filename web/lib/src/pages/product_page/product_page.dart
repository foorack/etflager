import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:etflager_common/src/api/model/product.dart';
import 'package:etflager_web/app.dart';
import 'package:etflager_web/src/components/product_card/product_card_price.dart';
import 'package:etflager_web/src/components/product_stock_status/product_stock_status.dart';
import 'package:etflager_web/src/route_paths.dart';

@Component(
  selector: 'product-page',
  styleUrls: ['product_page.css'],
  templateUrl: 'product_page.html',
  directives: [
    NgIf,
    routerDirectives,
    ProductStockStatusComponent,
    ProductCardPriceComponent
  ],
  exports: [RoutePaths],
)
class ProductPageComponent implements OnActivate {
  final Router _router;
  ProductPageComponent(this._router);
  Product product;

  @override
  Future<void> onActivate(RouterState previous, RouterState current) async {
    int artnr = int.tryParse(current.parameters['artnr']);
    if (artnr == null) {
      await _router.navigate(RoutePaths.c404.toUrl());
      return;
    }
    product = await ETFLagerApp.api.getProduct(artnr);
  }
}
