import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:etflager_common/src/api/model/product.dart';
import 'package:etflager_web/app.dart';
import 'package:etflager_web/src/components/product_card/product_card.dart';
import 'package:etflager_web/src/route_paths.dart';

@Component(
  selector: 'home-page',
  styleUrls: ['home_page.css'],
  templateUrl: 'home_page.html',
  directives: [NgFor, routerDirectives, ProductCardComponent],
  exports: [RoutePaths],
)
class HomePageComponent implements OnActivate {
  List<Product> products = List();

  @override
  Future<void> onActivate(RouterState previous, RouterState current) async {
    products = await ETFLagerApp.api.getProducts();
  }
}
