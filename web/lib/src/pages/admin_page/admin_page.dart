import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:etflager_web/src/route_paths.dart';

@Component(
  selector: 'admin-page',
  styleUrls: ['admin_page.css'],
  templateUrl: 'admin_page.html',
  directives: [routerDirectives],
  exports: [RoutePaths],
)
class AdminPageComponent {
}
