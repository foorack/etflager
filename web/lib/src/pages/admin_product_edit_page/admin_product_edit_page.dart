import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:etflager_common/src/api/model/product.dart';
import 'package:etflager_web/app.dart';
import 'package:etflager_web/src/components/product_editor/product_editor.dart';
import 'package:etflager_web/src/route_paths.dart';

@Component(
  selector: 'admin-product-edit-page',
  styleUrls: ['admin_product_edit_page.css'],
  templateUrl: 'admin_product_edit_page.html',
  directives: [NgIf, routerDirectives, ProductEditorComponent],
  exports: [RoutePaths],
)
class AdminProductEditPageComponent implements OnActivate {
  final Router _router;
  AdminProductEditPageComponent(this._router);
  Product product;

  @override
  Future<void> onActivate(RouterState previous, RouterState current) async {
    int artnr = int.tryParse(current.parameters['artnr']);
    if (artnr == null) {
      await _router.navigate(RoutePaths.c404.toUrl());
      return;
    }
    product = await ETFLagerApp.api.getProduct(artnr);
  }
  
}
