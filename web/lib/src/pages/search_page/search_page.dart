import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:etflager_common/src/api/model/product.dart';
import 'package:etflager_web/app.dart';
import 'package:etflager_web/src/components/product_card/product_card.dart';
import 'package:etflager_web/src/route_paths.dart';

@Component(
  selector: 'search-page',
  styleUrls: ['search_page.css'],
  templateUrl: 'search_page.html',
  directives: [NgFor, routerDirectives, ProductCardComponent],
  exports: [RoutePaths],
)
class SearchPageComponent implements OnActivate {
  String searchQuery = "";
  List<Product> products = List();

  @override
  Future<void> onActivate(RouterState previous, RouterState current) async {
    searchQuery = current.queryParameters['q'] == null
        ? ""
        : current.queryParameters['q'];
    products = await ETFLagerApp.api.getProducts(search: searchQuery);
  }
}
