import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:etflager_web/src/components/product_editor/product_editor.dart';
import 'package:etflager_web/src/route_paths.dart';

@Component(
  selector: 'admin-product-add-page',
  styleUrls: ['admin_product_add_page.css'],
  templateUrl: 'admin_product_add_page.html',
  directives: [routerDirectives, ProductEditorComponent],
  exports: [RoutePaths],
)
class AdminProductAddPageComponent {
}
