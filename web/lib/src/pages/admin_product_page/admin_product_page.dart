import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:etflager_common/src/api/model/product.dart';
import 'package:etflager_web/app.dart';
import 'package:etflager_web/src/components/product_card/product_card_price.dart';
import 'package:etflager_web/src/components/product_stock_status/product_stock_status.dart';
import 'package:etflager_web/src/route_paths.dart';

@Component(
  selector: 'admin-product-page',
  styleUrls: ['admin_product_page.css'],
  templateUrl: 'admin_product_page.html',
  directives: [
    NgFor,
    routerDirectives,
    ProductStockStatusComponent,
    ProductCardPriceComponent
  ],
  exports: [RoutePaths],
)
class AdminProductPageComponent implements OnActivate {
  List<Product> products = List();

  @override
  Future<void> onActivate(RouterState previous, RouterState current) async {
    products = await ETFLagerApp.api.getProducts();
  }

  String gotoProduct(int artnr) {
    return RoutePaths.product.toUrl(parameters: {'artnr': artnr.toString()});
  }

  String editProduct(int artnr) {
    return RoutePaths.admin_products_edit
        .toUrl(parameters: {'artnr': artnr.toString()});
  }

  void deleteProduct(int artnr) async {
    await ETFLagerApp.api.deleteProduct(artnr);
    products = await ETFLagerApp.api.getProducts();
  }
}
