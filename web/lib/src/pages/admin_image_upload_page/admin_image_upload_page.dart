import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'dart:typed_data';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:etflager_web/app.dart';
import 'package:etflager_web/src/route_paths.dart';
import 'package:etflager_common/src/api/model/product_image.dart';

@Component(
  selector: 'admin-image-upload-page',
  styleUrls: ['admin_image_upload_page.css'],
  templateUrl: 'admin_image_upload_page.html',
  directives: [NgIf, NgFor, routerDirectives],
  exports: [RoutePaths],
)
class AdminImageUploadPageComponent implements OnActivate {
  List<ProductImage> images = List();

  @override
  Future<void> onActivate(RouterState previous, RouterState current) async {}

  void requestFiles() {
    FileUploadInputElement input = querySelector("#adminImageUpload");
    input.click();
  }

  void uploadFiles(FileList files) {
    // Start all files async
    for (File file in files) {
      processFile(file);
    }
  }

  Future<void> processFile(File file) async {
    print("Processing file: ${file.name}");
    // Create visual card
    String name = file.name.split('.').first;
    ProductImage ui = ProductImage(0, name, null);
    images.add(ui);

    // Read file into memory
    FileReader reader = FileReader();
    reader.readAsArrayBuffer(file);

    ui.progress = 10;

    // Wait for file to be read
    await reader.onLoadEnd.first;

    ui.progress = 50;

    // Send image to server
    Uint8List data = reader.result as Uint8List;
    var enc = base64.encode(data);

    ui.progress = 60;

    ProductImage nui = await ETFLagerApp.api.uploadImage(ProductImage(0, name, enc));
    ui.imageData = nui.imageData;

    ui.progress = 100;
  }
}