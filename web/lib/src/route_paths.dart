import 'package:angular_router/angular_router.dart';

class RoutePaths {
  static final c404 = RoutePath(path: '404');

  static final home = RoutePath(path: '');
  static final search = RoutePath(path: 'search');
  static final product = RoutePath(path: 'product/:artnr');

  static final admin_home = RoutePath(path: 'admin');
  static final admin_products = RoutePath(path: 'admin/products');
  static final admin_products_add = RoutePath(path: 'admin/products/add');
  static final admin_products_edit = RoutePath(path: 'admin/products/edit/:artnr');
  static final admin_images = RoutePath(path: 'admin/images');
  static final admin_images_upload = RoutePath(path: 'admin/images/upload');
  static final admin_sales = RoutePath(path: 'admin/sales');
  static final admin_categories = RoutePath(path: 'admin/categories');
  static final admin_tags = RoutePath(path: 'admin/tags');
  static final admin_news = RoutePath(path: 'admin/news');
}