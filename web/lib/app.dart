import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:etflager_common/src/api/lagerapi.dart';
import 'package:etflager_web/src/components/header/header.dart';

import 'package:etflager_web/src/routes.dart';

@Component(
  selector: 'etflager-app',
  styleUrls: ['app.css'],
  templateUrl: 'app.html',
  directives: [HeaderComponent, routerDirectives],
  exports: [RoutePaths, Routes],
)
class ETFLagerApp {
  static LagerAPI api = LagerAPI();
  // Nothing here yet.
}
