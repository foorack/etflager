library etflager_common;

import 'dart:convert';

import 'package:etflager_common/src/api/model/product.dart';
import 'package:etflager_common/src/api/model/product_image.dart';
import 'package:http/http.dart' as http;

Uri base = Uri.parse('http://localhost:3000/api/v1/');

class LagerAPI {
  var client = http.Client();

  Future<List<Product>> getProducts({String search, int page, int per_page}) async {
    Uri uri = base.replace(path: base.path + 'products');
    if (search != null) {
      uri = uri.replace(queryParameters: {...uri.queryParameters, 'search': search});
    }
    if (page != null) {
      uri = uri.replace(queryParameters: {...uri.queryParameters, 'page': page});
    }
    if (per_page != null) {
      uri = uri.replace(queryParameters: {...uri.queryParameters, 'per_page': per_page});
    }
    String raw = (await http.get(uri)).body;
    dynamic data = jsonDecode(raw);
    List<Product> products = List();
    for (dynamic p in data) {
      products.add(Product.decode(p));
    }
    return products;
  }

  Future<Product> getProduct(int artnr) async {
    Uri uri = base.replace(path: base.path + 'products/' + artnr.toString());
    String raw = (await http.get(uri)).body;
    dynamic data = jsonDecode(raw);
    return Product.decode(data);
  }

  Future<Product> addProduct(Product product) async {
    Uri uri = base.replace(path: base.path + 'products');
    String raw = (await http.post(uri,
            headers: {'content-type': 'application/json'},
            body: jsonEncode(Product.encode(product))))
        .body;
    dynamic data = jsonDecode(raw);
    return Product.decode(data);
  }

  Future<Product> updateProduct(Product product) async {
    Uri uri = base.replace(path: base.path + 'products');
    String raw = (await http.put(uri,
            headers: {'content-type': 'application/json'},
            body: jsonEncode(Product.encode(product))))
        .body;
    dynamic data = jsonDecode(raw);
    return Product.decode(data);
  }

  Future<Product> deleteProduct(int artnr) async {
    Uri uri = base.replace(path: base.path + 'products/' + artnr.toString());
    String raw = (await http.delete(uri)).body;
    dynamic data = jsonDecode(raw);
    return Product.decode(data);
  }

  Future<ProductImage> uploadImage(ProductImage image) async {
    Uri uri = base.replace(path: base.path + 'images');
    String raw = (await http.post(uri,
            headers: {'content-type': 'application/json'},
            body: jsonEncode(ProductImage.encode(image))))
        .body;
    dynamic data = jsonDecode(raw);
    return ProductImage.decode(data);
  }

  Future<List<ProductImage>> getImages({String search, int page, int per_page}) async {
    Uri uri = base.replace(path: base.path + 'images');
    if (search != null) {
      uri = uri.replace(queryParameters: {...uri.queryParameters, 'search': search});
    }
    if (page != null) {
      uri = uri.replace(queryParameters: {...uri.queryParameters, 'page': page});
    }
    if (per_page != null) {
      uri = uri.replace(queryParameters: {...uri.queryParameters, 'per_page': per_page});
    }
    String raw = (await http.get(uri)).body;
    dynamic data = jsonDecode(raw);
    List<ProductImage> images = List();
    for (dynamic img in data) {
      images.add(ProductImage.decode(img));
    }
    return images;
  }

Future<ProductImage> updateImage(ProductImage image) async {
    Uri uri = base.replace(path: base.path + 'images');
    String raw = (await http.put(uri,
            headers: {'content-type': 'application/json'},
            body: jsonEncode(ProductImage.encode(image))))
        .body;
    dynamic data = jsonDecode(raw);
    return ProductImage.decode(data);
  }

  Future<ProductImage> deleteImage(int imgnr) async {
    Uri uri = base.replace(path: base.path + 'images/' + imgnr.toString());
    String raw = (await http.delete(uri)).body;
    dynamic data = jsonDecode(raw);
    return ProductImage.decode(data);
  }
}
