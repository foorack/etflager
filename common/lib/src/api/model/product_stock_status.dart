class ProductStockStatus {
  bool available;
  bool unknown;

  ProductStockStatus(this.available, this.unknown);

  String getColor() {
    if (unknown) {
      return 'orange';
    }
    return available ? 'green' : 'red';
  }

  String getText() {
    if (unknown) {
      return 'OKÄNT / FRÅGA';
    }
    return available ? 'FINNS I LAGER' : 'SLUT PÅ LAGER';
  }

  static ProductStockStatus decode(dynamic object) {
    assert(object is Map);
    assert(object['available'] is bool);
    assert(object['unknown'] is bool);
    return ProductStockStatus(object['available'], object['unknown']);
  }

  static Map<String, dynamic> encode(ProductStockStatus stockStatus) {
    return {
      'available': stockStatus.available,
      'unknown': stockStatus.unknown,
    };
  }
}
