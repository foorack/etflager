/// A single image can be attached to multiple products.
class ProductImage {
  int imgnr = 0;
  String name, imageData;

  // optional, not to be included in encoding/decoding
  int progress = 0;

  ProductImage(this.imgnr, this.name, this.imageData);

  ProductImage.empty() {
    name = "";
    imageData = "";
  }

  static ProductImage decode(dynamic object) {
    assert(object is Map);
    assert(object['imgnr'] is int);
    assert(object['name'] is String);
    assert(object['imageData'] is String);
    return ProductImage(object['imgnr'], object['name'], object['imageData']);
  }

  static Map<String, dynamic> encode(ProductImage image) {
    return {
      'imgnr': image.imgnr,
      'name': image.name,
      'imageData': image.imageData,
    };
  }
}
