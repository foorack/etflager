import 'package:etflager_common/src/api/model/product_image.dart';
import 'package:etflager_common/src/api/model/product_stock_status.dart';

class Product {
  int artnr = 0;
  String title, shortDescription, longDescription;
  ProductStockStatus stockStatus;
  double price;
  bool priceUnknown;
  ProductImage image;

  Product.empty() {
    title = '';
    shortDescription = '';
    longDescription = '';
    price = 0;
    priceUnknown = true;
    stockStatus = ProductStockStatus(true, false);
    image = ProductImage.empty();
  }

  Product(this.artnr, this.title, this.shortDescription, this.longDescription,
      this.stockStatus, this.price, this.priceUnknown, this.image);

  static Product decode(dynamic object) {
    assert(object is Map);
    assert(object['artnr'] is int);
    assert(object['title'] is String);
    assert(object['stock'] is Map);
    assert(object['price'] is double || object['price'] is int);
    assert(object['priceUnknown'] is bool);
    assert(object['image'] is Map);
    return Product(
        object['artnr'],
        object['title'],
        object['shortDescription'],
        object['longDescription'],
        ProductStockStatus.decode(object['stock']),
        double.parse(object['price'].toString()),
        object['priceUnknown'],
        ProductImage.decode(object['image']));
  }

  static Map<String, dynamic> encode(Product product) {
    return {
      'artnr': product.artnr,
      'title': product.title,
      'shortDescription': product.shortDescription,
      'longDescription': product.longDescription,
      'stock': ProductStockStatus.encode(product.stockStatus),
      'price': product.price,
      'priceUnknown': product.priceUnknown,
      'image': ProductImage.encode(product.image),
    };
  }
}
